"use strict";
var core_1 = require('@angular/core');
var http_1 = require('@angular/http');
var testing_1 = require('@angular/http/testing');
var Observable_1 = require('rxjs/Observable');
var resource_list_service_1 = require('./resource-list.service');
function main() {
    describe('NameList Service', function () {
        var nameListService;
        var mockBackend;
        var initialResponse;
        beforeEach(function () {
            var injector = core_1.ReflectiveInjector.resolveAndCreate([
                resource_list_service_1.ResourceListService,
                http_1.BaseRequestOptions,
                testing_1.MockBackend,
                { provide: http_1.Http,
                    useFactory: function (backend, defaultOptions) {
                        return new http_1.Http(backend, defaultOptions);
                    },
                    deps: [testing_1.MockBackend, http_1.BaseRequestOptions]
                },
            ]);
            nameListService = injector.get(NameListService);
            mockBackend = injector.get(testing_1.MockBackend);
            var connection;
            mockBackend.connections.subscribe(function (c) { return connection = c; });
            initialResponse = nameListService.get();
            connection.mockRespond(new http_1.Response(new http_1.ResponseOptions({ body: '["Dijkstra", "Hopper"]' })));
        });
        it('should return an Observable when get called', function () {
            expect(initialResponse).toEqual(jasmine.any(Observable_1.Observable));
        });
        it('should resolve to list of names when get called', function () {
            var names;
            initialResponse.subscribe(function (data) { return names = data; });
            expect(names).toEqual(['Dijkstra', 'Hopper']);
        });
    });
}
exports.main = main;

//# sourceMappingURL=data:application/json;charset=utf8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFwcC9zaGFyZWQvcmVzb3VyY2UtbGlzdC9yZXNvdXJjZS1saXN0LnNlcnZpY2Uuc3BlYy50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiO0FBQUEscUJBQW1DLGVBQWUsQ0FBQyxDQUFBO0FBQ25ELHFCQUF1RixlQUFlLENBQUMsQ0FBQTtBQUN2Ryx3QkFBNEIsdUJBQXVCLENBQUMsQ0FBQTtBQUNwRCwyQkFBMkIsaUJBQWlCLENBQUMsQ0FBQTtBQUU3QyxzQ0FBb0MseUJBQXlCLENBQUMsQ0FBQTtBQUU5RDtJQUNFLFFBQVEsQ0FBQyxrQkFBa0IsRUFBRTtRQUMzQixJQUFJLGVBQW9DLENBQUM7UUFDekMsSUFBSSxXQUF3QixDQUFDO1FBQzdCLElBQUksZUFBb0IsQ0FBQztRQUV6QixVQUFVLENBQUM7WUFFVCxJQUFJLFFBQVEsR0FBRyx5QkFBa0IsQ0FBQyxnQkFBZ0IsQ0FBQztnQkFDakQsMkNBQW1CO2dCQUNuQix5QkFBa0I7Z0JBQ2xCLHFCQUFXO2dCQUNYLEVBQUMsT0FBTyxFQUFFLFdBQUk7b0JBQ1osVUFBVSxFQUFFLFVBQVMsT0FBMEIsRUFBRSxjQUFrQzt3QkFDakYsTUFBTSxDQUFDLElBQUksV0FBSSxDQUFDLE9BQU8sRUFBRSxjQUFjLENBQUMsQ0FBQztvQkFDM0MsQ0FBQztvQkFDRCxJQUFJLEVBQUUsQ0FBQyxxQkFBVyxFQUFFLHlCQUFrQixDQUFDO2lCQUN4QzthQUNGLENBQUMsQ0FBQztZQUNILGVBQWUsR0FBRyxRQUFRLENBQUMsR0FBRyxDQUFDLGVBQWUsQ0FBQyxDQUFDO1lBQ2hELFdBQVcsR0FBRyxRQUFRLENBQUMsR0FBRyxDQUFDLHFCQUFXLENBQUMsQ0FBQztZQUV4QyxJQUFJLFVBQWUsQ0FBQztZQUNwQixXQUFXLENBQUMsV0FBVyxDQUFDLFNBQVMsQ0FBQyxVQUFDLENBQU0sSUFBSyxPQUFBLFVBQVUsR0FBRyxDQUFDLEVBQWQsQ0FBYyxDQUFDLENBQUM7WUFDOUQsZUFBZSxHQUFHLGVBQWUsQ0FBQyxHQUFHLEVBQUUsQ0FBQztZQUN4QyxVQUFVLENBQUMsV0FBVyxDQUFDLElBQUksZUFBUSxDQUFDLElBQUksc0JBQWUsQ0FBQyxFQUFFLElBQUksRUFBRSx3QkFBd0IsRUFBRSxDQUFDLENBQUMsQ0FBQyxDQUFDO1FBQ2hHLENBQUMsQ0FBQyxDQUFDO1FBRUgsRUFBRSxDQUFDLDZDQUE2QyxFQUFFO1lBQ2hELE1BQU0sQ0FBQyxlQUFlLENBQUMsQ0FBQyxPQUFPLENBQUMsT0FBTyxDQUFDLEdBQUcsQ0FBQyx1QkFBVSxDQUFDLENBQUMsQ0FBQztRQUMzRCxDQUFDLENBQUMsQ0FBQztRQUVILEVBQUUsQ0FBQyxpREFBaUQsRUFBRTtZQUNwRCxJQUFJLEtBQVUsQ0FBQztZQUNmLGVBQWUsQ0FBQyxTQUFTLENBQUMsVUFBQyxJQUFTLElBQUssT0FBQSxLQUFLLEdBQUcsSUFBSSxFQUFaLENBQVksQ0FBQyxDQUFDO1lBQ3ZELE1BQU0sQ0FBQyxLQUFLLENBQUMsQ0FBQyxPQUFPLENBQUMsQ0FBQyxVQUFVLEVBQUUsUUFBUSxDQUFDLENBQUMsQ0FBQztRQUNoRCxDQUFDLENBQUMsQ0FBQztJQUNMLENBQUMsQ0FBQyxDQUFDO0FBQ0wsQ0FBQztBQXRDZSxZQUFJLE9Bc0NuQixDQUFBIiwiZmlsZSI6ImFwcC9zaGFyZWQvcmVzb3VyY2UtbGlzdC9yZXNvdXJjZS1saXN0LnNlcnZpY2Uuc3BlYy5qcyIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IFJlZmxlY3RpdmVJbmplY3RvciB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xuaW1wb3J0IHsgQmFzZVJlcXVlc3RPcHRpb25zLCBDb25uZWN0aW9uQmFja2VuZCwgSHR0cCwgUmVzcG9uc2UsIFJlc3BvbnNlT3B0aW9ucyB9IGZyb20gJ0Bhbmd1bGFyL2h0dHAnO1xuaW1wb3J0IHsgTW9ja0JhY2tlbmQgfSBmcm9tICdAYW5ndWxhci9odHRwL3Rlc3RpbmcnO1xuaW1wb3J0IHsgT2JzZXJ2YWJsZSB9IGZyb20gJ3J4anMvT2JzZXJ2YWJsZSc7XG5cbmltcG9ydCB7IFJlc291cmNlTGlzdFNlcnZpY2UgfSBmcm9tICcuL3Jlc291cmNlLWxpc3Quc2VydmljZSc7XG5cbmV4cG9ydCBmdW5jdGlvbiBtYWluKCkge1xuICBkZXNjcmliZSgnTmFtZUxpc3QgU2VydmljZScsICgpID0+IHtcbiAgICBsZXQgbmFtZUxpc3RTZXJ2aWNlOiBSZXNvdXJjZUxpc3RTZXJ2aWNlO1xuICAgIGxldCBtb2NrQmFja2VuZDogTW9ja0JhY2tlbmQ7XG4gICAgbGV0IGluaXRpYWxSZXNwb25zZTogYW55O1xuXG4gICAgYmVmb3JlRWFjaCgoKSA9PiB7XG5cbiAgICAgIGxldCBpbmplY3RvciA9IFJlZmxlY3RpdmVJbmplY3Rvci5yZXNvbHZlQW5kQ3JlYXRlKFtcbiAgICAgICAgUmVzb3VyY2VMaXN0U2VydmljZSxcbiAgICAgICAgQmFzZVJlcXVlc3RPcHRpb25zLFxuICAgICAgICBNb2NrQmFja2VuZCxcbiAgICAgICAge3Byb3ZpZGU6IEh0dHAsXG4gICAgICAgICAgdXNlRmFjdG9yeTogZnVuY3Rpb24oYmFja2VuZDogQ29ubmVjdGlvbkJhY2tlbmQsIGRlZmF1bHRPcHRpb25zOiBCYXNlUmVxdWVzdE9wdGlvbnMpIHtcbiAgICAgICAgICAgIHJldHVybiBuZXcgSHR0cChiYWNrZW5kLCBkZWZhdWx0T3B0aW9ucyk7XG4gICAgICAgICAgfSxcbiAgICAgICAgICBkZXBzOiBbTW9ja0JhY2tlbmQsIEJhc2VSZXF1ZXN0T3B0aW9uc11cbiAgICAgICAgfSxcbiAgICAgIF0pO1xuICAgICAgbmFtZUxpc3RTZXJ2aWNlID0gaW5qZWN0b3IuZ2V0KE5hbWVMaXN0U2VydmljZSk7XG4gICAgICBtb2NrQmFja2VuZCA9IGluamVjdG9yLmdldChNb2NrQmFja2VuZCk7XG5cbiAgICAgIGxldCBjb25uZWN0aW9uOiBhbnk7XG4gICAgICBtb2NrQmFja2VuZC5jb25uZWN0aW9ucy5zdWJzY3JpYmUoKGM6IGFueSkgPT4gY29ubmVjdGlvbiA9IGMpO1xuICAgICAgaW5pdGlhbFJlc3BvbnNlID0gbmFtZUxpc3RTZXJ2aWNlLmdldCgpO1xuICAgICAgY29ubmVjdGlvbi5tb2NrUmVzcG9uZChuZXcgUmVzcG9uc2UobmV3IFJlc3BvbnNlT3B0aW9ucyh7IGJvZHk6ICdbXCJEaWprc3RyYVwiLCBcIkhvcHBlclwiXScgfSkpKTtcbiAgICB9KTtcblxuICAgIGl0KCdzaG91bGQgcmV0dXJuIGFuIE9ic2VydmFibGUgd2hlbiBnZXQgY2FsbGVkJywgKCkgPT4ge1xuICAgICAgZXhwZWN0KGluaXRpYWxSZXNwb25zZSkudG9FcXVhbChqYXNtaW5lLmFueShPYnNlcnZhYmxlKSk7XG4gICAgfSk7XG5cbiAgICBpdCgnc2hvdWxkIHJlc29sdmUgdG8gbGlzdCBvZiBuYW1lcyB3aGVuIGdldCBjYWxsZWQnLCAoKSA9PiB7XG4gICAgICBsZXQgbmFtZXM6IGFueTtcbiAgICAgIGluaXRpYWxSZXNwb25zZS5zdWJzY3JpYmUoKGRhdGE6IGFueSkgPT4gbmFtZXMgPSBkYXRhKTtcbiAgICAgIGV4cGVjdChuYW1lcykudG9FcXVhbChbJ0RpamtzdHJhJywgJ0hvcHBlciddKTtcbiAgICB9KTtcbiAgfSk7XG59XG4iXX0=
