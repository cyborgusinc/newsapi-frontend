"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var http_1 = require('@angular/http');
var Observable_1 = require('rxjs/Observable');
require('rxjs/add/observable/throw');
var ArticleListService = (function () {
    function ArticleListService(http) {
        this.http = http;
        this.apiKey = "64f848050c93450aa5b08b082eaa791b";
    }
    ArticleListService.prototype.get = function (source) {
        console.log("calling it");
        return this.http.get('https://newsapi.org/v1/artocles?source=' + source.name + '&sortBy=latest&apiKey=' + this.apiKey)
            .map(function (res) { return res.json().articles; })
            .catch(this.handleError);
    };
    ArticleListService.prototype.handleError = function (error) {
        var errMsg = (error.message) ? error.message :
            error.status ? error.status + " - " + error.statusText : 'Server error';
        console.error(errMsg);
        return Observable_1.Observable.throw(errMsg);
    };
    ArticleListService = __decorate([
        core_1.Injectable(), 
        __metadata('design:paramtypes', [http_1.Http])
    ], ArticleListService);
    return ArticleListService;
}());
exports.ArticleListService = ArticleListService;

//# sourceMappingURL=data:application/json;charset=utf8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFwcC9zaGFyZWQvYXJ0aWNsZXMtbGlzdC9hcnRpY2xlLWxpc3Quc2VydmljZS50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7Ozs7Ozs7O0FBQUEscUJBQTJCLGVBQWUsQ0FBQyxDQUFBO0FBQzNDLHFCQUFzRCxlQUFlLENBQUMsQ0FBQTtBQUN0RSwyQkFBMkIsaUJBQWlCLENBQUMsQ0FBQTtBQUU3QyxRQUFPLDJCQUEyQixDQUFDLENBQUE7QUFRbkM7SUFTRSw0QkFBb0IsSUFBVTtRQUFWLFNBQUksR0FBSixJQUFJLENBQU07UUFQdEIsV0FBTSxHQUFHLGtDQUFrQyxDQUFDO0lBT25CLENBQUM7SUFNbEMsZ0NBQUcsR0FBSCxVQUFJLE1BQWM7UUFDaEIsT0FBTyxDQUFDLEdBQUcsQ0FBQyxZQUFZLENBQUMsQ0FBQTtRQUN6QixNQUFNLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxHQUFHLENBQUMseUNBQXlDLEdBQUUsTUFBTSxDQUFDLElBQUksR0FBRSx3QkFBd0IsR0FBRSxJQUFJLENBQUMsTUFBTSxDQUFDO2FBQ2hILEdBQUcsQ0FBQyxVQUFDLEdBQWEsSUFBSyxPQUFZLEdBQUcsQ0FBQyxJQUFJLEVBQUUsQ0FBQyxRQUFRLEVBQS9CLENBQStCLENBQUM7YUFFdkQsS0FBSyxDQUFDLElBQUksQ0FBQyxXQUFXLENBQUMsQ0FBQztJQUM3QixDQUFDO0lBT08sd0NBQVcsR0FBbkIsVUFBcUIsS0FBVTtRQUc3QixJQUFJLE1BQU0sR0FBRyxDQUFDLEtBQUssQ0FBQyxPQUFPLENBQUMsR0FBRyxLQUFLLENBQUMsT0FBTztZQUMxQyxLQUFLLENBQUMsTUFBTSxHQUFNLEtBQUssQ0FBQyxNQUFNLFdBQU0sS0FBSyxDQUFDLFVBQVksR0FBRyxjQUFjLENBQUM7UUFDMUUsT0FBTyxDQUFDLEtBQUssQ0FBQyxNQUFNLENBQUMsQ0FBQztRQUN0QixNQUFNLENBQUMsdUJBQVUsQ0FBQyxLQUFLLENBQUMsTUFBTSxDQUFDLENBQUM7SUFDbEMsQ0FBQztJQXBDSDtRQUFDLGlCQUFVLEVBQUU7OzBCQUFBO0lBdUNiLHlCQUFDO0FBQUQsQ0F0Q0EsQUFzQ0MsSUFBQTtBQXRDWSwwQkFBa0IscUJBc0M5QixDQUFBIiwiZmlsZSI6ImFwcC9zaGFyZWQvYXJ0aWNsZXMtbGlzdC9hcnRpY2xlLWxpc3Quc2VydmljZS5qcyIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IEluamVjdGFibGUgfSBmcm9tICdAYW5ndWxhci9jb3JlJztcbmltcG9ydCB7SHR0cCwgUmVzcG9uc2UsIEhlYWRlcnMsIFJlcXVlc3RPcHRpb25zfSBmcm9tICdAYW5ndWxhci9odHRwJztcbmltcG9ydCB7IE9ic2VydmFibGUgfSBmcm9tICdyeGpzL09ic2VydmFibGUnO1xuXG5pbXBvcnQgJ3J4anMvYWRkL29ic2VydmFibGUvdGhyb3cnO1xuaW1wb3J0IHtBcnRpY2xlfSBmcm9tIFwiLi4vLi4vLi4vbW9kZWxzL2FydGljbGVcIjtcbmltcG9ydCB7U291cmNlfSBmcm9tIFwiLi4vLi4vLi4vbW9kZWxzL3NvdXJjZVwiO1xuXG4vKipcbiAqIFRoaXMgY2xhc3MgcHJvdmlkZXMgdGhlIE5hbWVMaXN0IHNlcnZpY2Ugd2l0aCBtZXRob2RzIHRvIHJlYWQgbmFtZXMgYW5kIGFkZCBuYW1lcy5cbiAqL1xuQEluamVjdGFibGUoKVxuZXhwb3J0IGNsYXNzIEFydGljbGVMaXN0U2VydmljZSB7XG5cbiAgcHJpdmF0ZSBhcGlLZXkgPSBcIjY0Zjg0ODA1MGM5MzQ1MGFhNWIwOGIwODJlYWE3OTFiXCI7XG5cbiAgLyoqXG4gICAqIENyZWF0ZXMgYSBuZXcgTmFtZUxpc3RTZXJ2aWNlIHdpdGggdGhlIGluamVjdGVkIEh0dHAuXG4gICAqIEBwYXJhbSB7SHR0cH0gaHR0cCAtIFRoZSBpbmplY3RlZCBIdHRwLlxuICAgKiBAY29uc3RydWN0b3JcbiAgICovXG4gIGNvbnN0cnVjdG9yKHByaXZhdGUgaHR0cDogSHR0cCkge31cblxuICAvKipcbiAgICogUmV0dXJucyBhbiBPYnNlcnZhYmxlIGZvciB0aGUgSFRUUCBHRVQgcmVxdWVzdCBmb3IgdGhlIEpTT04gcmVzb3VyY2UuXG4gICAqIEByZXR1cm4ge3N0cmluZ1tdfSBUaGUgT2JzZXJ2YWJsZSBmb3IgdGhlIEhUVFAgcmVxdWVzdC5cbiAgICovXG4gIGdldChzb3VyY2U6IFNvdXJjZSk6IE9ic2VydmFibGU8U291cmNlW10+IHtcbiAgICBjb25zb2xlLmxvZyhcImNhbGxpbmcgaXRcIilcbiAgICByZXR1cm4gdGhpcy5odHRwLmdldCgnaHR0cHM6Ly9uZXdzYXBpLm9yZy92MS9hcnRvY2xlcz9zb3VyY2U9Jysgc291cmNlLm5hbWUgKycmc29ydEJ5PWxhdGVzdCZhcGlLZXk9JysgdGhpcy5hcGlLZXkpXG4gICAgICAubWFwKChyZXM6IFJlc3BvbnNlKSA9PiA8QXJ0aWNsZVtdPiByZXMuanNvbigpLmFydGljbGVzKVxuICAgICAgLy8gICAgICAgICAgICAgIC5kbyhkYXRhID0+IGNvbnNvbGUubG9nKCdzZXJ2ZXIgZGF0YTonLCBkYXRhKSkgIC8vIGRlYnVnXG4gICAgICAuY2F0Y2godGhpcy5oYW5kbGVFcnJvcik7XG4gIH1cblxuXG5cbiAgLyoqXG4gICAqIEhhbmRsZSBIVFRQIGVycm9yXG4gICAqL1xuICBwcml2YXRlIGhhbmRsZUVycm9yIChlcnJvcjogYW55KSB7XG4gICAgLy8gSW4gYSByZWFsIHdvcmxkIGFwcCwgd2UgbWlnaHQgdXNlIGEgcmVtb3RlIGxvZ2dpbmcgaW5mcmFzdHJ1Y3R1cmVcbiAgICAvLyBXZSdkIGFsc28gZGlnIGRlZXBlciBpbnRvIHRoZSBlcnJvciB0byBnZXQgYSBiZXR0ZXIgbWVzc2FnZVxuICAgIGxldCBlcnJNc2cgPSAoZXJyb3IubWVzc2FnZSkgPyBlcnJvci5tZXNzYWdlIDpcbiAgICAgIGVycm9yLnN0YXR1cyA/IGAke2Vycm9yLnN0YXR1c30gLSAke2Vycm9yLnN0YXR1c1RleHR9YCA6ICdTZXJ2ZXIgZXJyb3InO1xuICAgIGNvbnNvbGUuZXJyb3IoZXJyTXNnKTsgLy8gbG9nIHRvIGNvbnNvbGUgaW5zdGVhZFxuICAgIHJldHVybiBPYnNlcnZhYmxlLnRocm93KGVyck1zZyk7XG4gIH1cblxuXG59XG5cbiJdfQ==
