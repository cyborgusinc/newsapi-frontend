"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var http_1 = require('@angular/http');
var Observable_1 = require('rxjs/Observable');
require('rxjs/add/observable/throw');
var ArticleListService = (function () {
    function ArticleListService(http) {
        this.http = http;
        this.apiKey = "64f848050c93450aa5b08b082eaa791b";
    }
    ArticleListService.prototype.get = function (source) {
        console.log("calling it");
        return this.http.get('https://newsapi.org/v1/artocles?source=' + source.name + '&sortBy=latest&apiKey=' + this.apiKey)
            .map(function (res) { return res.json().articles; })
            .catch(this.handleError);
    };
    ArticleListService.prototype.handleError = function (error) {
        var errMsg = (error.message) ? error.message :
            error.status ? error.status + " - " + error.statusText : 'Server error';
        console.error(errMsg);
        return Observable_1.Observable.throw(errMsg);
    };
    ArticleListService = __decorate([
        core_1.Injectable(), 
        __metadata('design:paramtypes', [http_1.Http])
    ], ArticleListService);
    return ArticleListService;
}());
exports.ArticleListService = ArticleListService;

//# sourceMappingURL=data:application/json;charset=utf8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFwcC9zaGFyZWQvYXJ0aWNsZXMtbGlzdC9hcnRpY2xlcy1saXN0LnNlcnZpY2UudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7OztBQUFBLHFCQUEyQixlQUFlLENBQUMsQ0FBQTtBQUMzQyxxQkFBc0QsZUFBZSxDQUFDLENBQUE7QUFDdEUsMkJBQTJCLGlCQUFpQixDQUFDLENBQUE7QUFFN0MsUUFBTywyQkFBMkIsQ0FBQyxDQUFBO0FBUW5DO0lBU0UsNEJBQW9CLElBQVU7UUFBVixTQUFJLEdBQUosSUFBSSxDQUFNO1FBUHRCLFdBQU0sR0FBRyxrQ0FBa0MsQ0FBQztJQU9uQixDQUFDO0lBTWxDLGdDQUFHLEdBQUgsVUFBSSxNQUFjO1FBQ2hCLE9BQU8sQ0FBQyxHQUFHLENBQUMsWUFBWSxDQUFDLENBQUE7UUFDekIsTUFBTSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsR0FBRyxDQUFDLHlDQUF5QyxHQUFFLE1BQU0sQ0FBQyxJQUFJLEdBQUUsd0JBQXdCLEdBQUUsSUFBSSxDQUFDLE1BQU0sQ0FBQzthQUNoSCxHQUFHLENBQUMsVUFBQyxHQUFhLElBQUssT0FBWSxHQUFHLENBQUMsSUFBSSxFQUFFLENBQUMsUUFBUSxFQUEvQixDQUErQixDQUFDO2FBRXZELEtBQUssQ0FBQyxJQUFJLENBQUMsV0FBVyxDQUFDLENBQUM7SUFDN0IsQ0FBQztJQU9PLHdDQUFXLEdBQW5CLFVBQXFCLEtBQVU7UUFHN0IsSUFBSSxNQUFNLEdBQUcsQ0FBQyxLQUFLLENBQUMsT0FBTyxDQUFDLEdBQUcsS0FBSyxDQUFDLE9BQU87WUFDMUMsS0FBSyxDQUFDLE1BQU0sR0FBTSxLQUFLLENBQUMsTUFBTSxXQUFNLEtBQUssQ0FBQyxVQUFZLEdBQUcsY0FBYyxDQUFDO1FBQzFFLE9BQU8sQ0FBQyxLQUFLLENBQUMsTUFBTSxDQUFDLENBQUM7UUFDdEIsTUFBTSxDQUFDLHVCQUFVLENBQUMsS0FBSyxDQUFDLE1BQU0sQ0FBQyxDQUFDO0lBQ2xDLENBQUM7SUFwQ0g7UUFBQyxpQkFBVSxFQUFFOzswQkFBQTtJQXVDYix5QkFBQztBQUFELENBdENBLEFBc0NDLElBQUE7QUF0Q1ksMEJBQWtCLHFCQXNDOUIsQ0FBQSIsImZpbGUiOiJhcHAvc2hhcmVkL2FydGljbGVzLWxpc3QvYXJ0aWNsZXMtbGlzdC5zZXJ2aWNlLmpzIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgSW5qZWN0YWJsZSB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xuaW1wb3J0IHtIdHRwLCBSZXNwb25zZSwgSGVhZGVycywgUmVxdWVzdE9wdGlvbnN9IGZyb20gJ0Bhbmd1bGFyL2h0dHAnO1xuaW1wb3J0IHsgT2JzZXJ2YWJsZSB9IGZyb20gJ3J4anMvT2JzZXJ2YWJsZSc7XG5cbmltcG9ydCAncnhqcy9hZGQvb2JzZXJ2YWJsZS90aHJvdyc7XG5pbXBvcnQge0FydGljbGV9IGZyb20gXCIuLi8uLi8uLi9tb2RlbHMvYXJ0aWNsZVwiO1xuaW1wb3J0IHtTb3VyY2V9IGZyb20gXCIuLi8uLi8uLi9tb2RlbHMvc291cmNlXCI7XG5cbi8qKlxuICogVGhpcyBjbGFzcyBwcm92aWRlcyB0aGUgTmFtZUxpc3Qgc2VydmljZSB3aXRoIG1ldGhvZHMgdG8gcmVhZCBuYW1lcyBhbmQgYWRkIG5hbWVzLlxuICovXG5ASW5qZWN0YWJsZSgpXG5leHBvcnQgY2xhc3MgQXJ0aWNsZUxpc3RTZXJ2aWNlIHtcblxuICBwcml2YXRlIGFwaUtleSA9IFwiNjRmODQ4MDUwYzkzNDUwYWE1YjA4YjA4MmVhYTc5MWJcIjtcblxuICAvKipcbiAgICogQ3JlYXRlcyBhIG5ldyBOYW1lTGlzdFNlcnZpY2Ugd2l0aCB0aGUgaW5qZWN0ZWQgSHR0cC5cbiAgICogQHBhcmFtIHtIdHRwfSBodHRwIC0gVGhlIGluamVjdGVkIEh0dHAuXG4gICAqIEBjb25zdHJ1Y3RvclxuICAgKi9cbiAgY29uc3RydWN0b3IocHJpdmF0ZSBodHRwOiBIdHRwKSB7fVxuXG4gIC8qKlxuICAgKiBSZXR1cm5zIGFuIE9ic2VydmFibGUgZm9yIHRoZSBIVFRQIEdFVCByZXF1ZXN0IGZvciB0aGUgSlNPTiByZXNvdXJjZS5cbiAgICogQHJldHVybiB7c3RyaW5nW119IFRoZSBPYnNlcnZhYmxlIGZvciB0aGUgSFRUUCByZXF1ZXN0LlxuICAgKi9cbiAgZ2V0KHNvdXJjZTogU291cmNlKTogT2JzZXJ2YWJsZTxTb3VyY2VbXT4ge1xuICAgIGNvbnNvbGUubG9nKFwiY2FsbGluZyBpdFwiKVxuICAgIHJldHVybiB0aGlzLmh0dHAuZ2V0KCdodHRwczovL25ld3NhcGkub3JnL3YxL2FydG9jbGVzP3NvdXJjZT0nKyBzb3VyY2UubmFtZSArJyZzb3J0Qnk9bGF0ZXN0JmFwaUtleT0nKyB0aGlzLmFwaUtleSlcbiAgICAgIC5tYXAoKHJlczogUmVzcG9uc2UpID0+IDxBcnRpY2xlW10+IHJlcy5qc29uKCkuYXJ0aWNsZXMpXG4gICAgICAvLyAgICAgICAgICAgICAgLmRvKGRhdGEgPT4gY29uc29sZS5sb2coJ3NlcnZlciBkYXRhOicsIGRhdGEpKSAgLy8gZGVidWdcbiAgICAgIC5jYXRjaCh0aGlzLmhhbmRsZUVycm9yKTtcbiAgfVxuXG5cblxuICAvKipcbiAgICogSGFuZGxlIEhUVFAgZXJyb3JcbiAgICovXG4gIHByaXZhdGUgaGFuZGxlRXJyb3IgKGVycm9yOiBhbnkpIHtcbiAgICAvLyBJbiBhIHJlYWwgd29ybGQgYXBwLCB3ZSBtaWdodCB1c2UgYSByZW1vdGUgbG9nZ2luZyBpbmZyYXN0cnVjdHVyZVxuICAgIC8vIFdlJ2QgYWxzbyBkaWcgZGVlcGVyIGludG8gdGhlIGVycm9yIHRvIGdldCBhIGJldHRlciBtZXNzYWdlXG4gICAgbGV0IGVyck1zZyA9IChlcnJvci5tZXNzYWdlKSA/IGVycm9yLm1lc3NhZ2UgOlxuICAgICAgZXJyb3Iuc3RhdHVzID8gYCR7ZXJyb3Iuc3RhdHVzfSAtICR7ZXJyb3Iuc3RhdHVzVGV4dH1gIDogJ1NlcnZlciBlcnJvcic7XG4gICAgY29uc29sZS5lcnJvcihlcnJNc2cpOyAvLyBsb2cgdG8gY29uc29sZSBpbnN0ZWFkXG4gICAgcmV0dXJuIE9ic2VydmFibGUudGhyb3coZXJyTXNnKTtcbiAgfVxuXG5cbn1cblxuIl19
