"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var http_1 = require('@angular/http');
var Observable_1 = require('rxjs/Observable');
require('rxjs/add/observable/throw');
var ArticleListService = (function () {
    function ArticleListService(http) {
        this.http = http;
        this.apiKey = "64f848050c93450aa5b08b082eaa791b";
    }
    ArticleListService.prototype.get = function (source) {
        return this.http.get('https://newsapi.org/v1/articles?source=' + source.id + '&sortBy=latest&apiKey=' + this.apiKey)
            .map(function (res) { return res.json().articles; })
            .catch(this.handleError);
    };
    ArticleListService.prototype.handleError = function (error) {
        var errMsg = (error.message) ? error.message :
            error.status ? error.status + " - " + error.statusText : 'Server error';
        console.error(errMsg);
        return Observable_1.Observable.throw(errMsg);
    };
    ArticleListService = __decorate([
        core_1.Injectable(), 
        __metadata('design:paramtypes', [http_1.Http])
    ], ArticleListService);
    return ArticleListService;
}());
exports.ArticleListService = ArticleListService;

//# sourceMappingURL=data:application/json;charset=utf8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFwcC9zaGFyZWQvYXJ0aWNsZS1saXN0L2FydGljbGUtbGlzdC5zZXJ2aWNlLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7Ozs7Ozs7QUFBQSxxQkFBMkIsZUFBZSxDQUFDLENBQUE7QUFDM0MscUJBQTZCLGVBQWUsQ0FBQyxDQUFBO0FBQzdDLDJCQUEyQixpQkFBaUIsQ0FBQyxDQUFBO0FBRTdDLFFBQU8sMkJBQTJCLENBQUMsQ0FBQTtBQVFuQztJQVNFLDRCQUFvQixJQUFVO1FBQVYsU0FBSSxHQUFKLElBQUksQ0FBTTtRQVB0QixXQUFNLEdBQUcsa0NBQWtDLENBQUM7SUFPbkIsQ0FBQztJQU1sQyxnQ0FBRyxHQUFILFVBQUksTUFBYztRQUNoQixNQUFNLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxHQUFHLENBQUMseUNBQXlDLEdBQUUsTUFBTSxDQUFDLEVBQUUsR0FBRSx3QkFBd0IsR0FBRSxJQUFJLENBQUMsTUFBTSxDQUFDO2FBQzlHLEdBQUcsQ0FBQyxVQUFDLEdBQWEsSUFBSyxPQUFZLEdBQUcsQ0FBQyxJQUFJLEVBQUUsQ0FBQyxRQUFRLEVBQS9CLENBQStCLENBQUM7YUFFdkQsS0FBSyxDQUFDLElBQUksQ0FBQyxXQUFXLENBQUMsQ0FBQztJQUM3QixDQUFDO0lBT08sd0NBQVcsR0FBbkIsVUFBcUIsS0FBVTtRQUc3QixJQUFJLE1BQU0sR0FBRyxDQUFDLEtBQUssQ0FBQyxPQUFPLENBQUMsR0FBRyxLQUFLLENBQUMsT0FBTztZQUMxQyxLQUFLLENBQUMsTUFBTSxHQUFNLEtBQUssQ0FBQyxNQUFNLFdBQU0sS0FBSyxDQUFDLFVBQVksR0FBRyxjQUFjLENBQUM7UUFDMUUsT0FBTyxDQUFDLEtBQUssQ0FBQyxNQUFNLENBQUMsQ0FBQztRQUN0QixNQUFNLENBQUMsdUJBQVUsQ0FBQyxLQUFLLENBQUMsTUFBTSxDQUFDLENBQUM7SUFDbEMsQ0FBQztJQW5DSDtRQUFDLGlCQUFVLEVBQUU7OzBCQUFBO0lBc0NiLHlCQUFDO0FBQUQsQ0FyQ0EsQUFxQ0MsSUFBQTtBQXJDWSwwQkFBa0IscUJBcUM5QixDQUFBIiwiZmlsZSI6ImFwcC9zaGFyZWQvYXJ0aWNsZS1saXN0L2FydGljbGUtbGlzdC5zZXJ2aWNlLmpzIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgSW5qZWN0YWJsZSB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xuaW1wb3J0IHtIdHRwLCBSZXNwb25zZX0gZnJvbSAnQGFuZ3VsYXIvaHR0cCc7XG5pbXBvcnQgeyBPYnNlcnZhYmxlIH0gZnJvbSAncnhqcy9PYnNlcnZhYmxlJztcblxuaW1wb3J0ICdyeGpzL2FkZC9vYnNlcnZhYmxlL3Rocm93JztcbmltcG9ydCB7QXJ0aWNsZX0gZnJvbSBcIi4uLy4uLy4uL21vZGVscy9hcnRpY2xlXCI7XG5pbXBvcnQge1NvdXJjZX0gZnJvbSBcIi4uLy4uLy4uL21vZGVscy9zb3VyY2VcIjtcblxuLyoqXG4gKiBUaGlzIGNsYXNzIHByb3ZpZGVzIHRoZSBBcnRpY2xlTGlzdCBzZXJ2aWNlIHdpdGggbWV0aG9kcyB0byByZWFkIG5hbWVzIGFuZCBhZGQgbmFtZXMuXG4gKi9cbkBJbmplY3RhYmxlKClcbmV4cG9ydCBjbGFzcyBBcnRpY2xlTGlzdFNlcnZpY2Uge1xuXG4gIHByaXZhdGUgYXBpS2V5ID0gXCI2NGY4NDgwNTBjOTM0NTBhYTViMDhiMDgyZWFhNzkxYlwiO1xuXG4gIC8qKlxuICAgKiBDcmVhdGVzIGEgbmV3IE5hbWVMaXN0U2VydmljZSB3aXRoIHRoZSBpbmplY3RlZCBIdHRwLlxuICAgKiBAcGFyYW0ge0h0dHB9IGh0dHAgLSBUaGUgaW5qZWN0ZWQgSHR0cC5cbiAgICogQGNvbnN0cnVjdG9yXG4gICAqL1xuICBjb25zdHJ1Y3Rvcihwcml2YXRlIGh0dHA6IEh0dHApIHt9XG5cbiAgLyoqXG4gICAqIFJldHVybnMgYW4gT2JzZXJ2YWJsZSBmb3IgdGhlIEhUVFAgR0VUIHJlcXVlc3QgZm9yIHRoZSBKU09OIHJlc291cmNlLlxuICAgKiBAcmV0dXJuIHtzdHJpbmdbXX0gVGhlIE9ic2VydmFibGUgZm9yIHRoZSBIVFRQIHJlcXVlc3QuXG4gICAqL1xuICBnZXQoc291cmNlOiBTb3VyY2UpOiBPYnNlcnZhYmxlPEFydGljbGVbXT4ge1xuICAgIHJldHVybiB0aGlzLmh0dHAuZ2V0KCdodHRwczovL25ld3NhcGkub3JnL3YxL2FydGljbGVzP3NvdXJjZT0nKyBzb3VyY2UuaWQgKycmc29ydEJ5PWxhdGVzdCZhcGlLZXk9JysgdGhpcy5hcGlLZXkpXG4gICAgICAubWFwKChyZXM6IFJlc3BvbnNlKSA9PiA8QXJ0aWNsZVtdPiByZXMuanNvbigpLmFydGljbGVzKVxuICAgICAgLy8gICAgICAgICAgICAgIC5kbyhkYXRhID0+IGNvbnNvbGUubG9nKCdzZXJ2ZXIgZGF0YTonLCBkYXRhKSkgIC8vIGRlYnVnXG4gICAgICAuY2F0Y2godGhpcy5oYW5kbGVFcnJvcik7XG4gIH1cblxuXG5cbiAgLyoqXG4gICAqIEhhbmRsZSBIVFRQIGVycm9yXG4gICAqL1xuICBwcml2YXRlIGhhbmRsZUVycm9yIChlcnJvcjogYW55KSB7XG4gICAgLy8gSW4gYSByZWFsIHdvcmxkIGFwcCwgd2UgbWlnaHQgdXNlIGEgcmVtb3RlIGxvZ2dpbmcgaW5mcmFzdHJ1Y3R1cmVcbiAgICAvLyBXZSdkIGFsc28gZGlnIGRlZXBlciBpbnRvIHRoZSBlcnJvciB0byBnZXQgYSBiZXR0ZXIgbWVzc2FnZVxuICAgIGxldCBlcnJNc2cgPSAoZXJyb3IubWVzc2FnZSkgPyBlcnJvci5tZXNzYWdlIDpcbiAgICAgIGVycm9yLnN0YXR1cyA/IGAke2Vycm9yLnN0YXR1c30gLSAke2Vycm9yLnN0YXR1c1RleHR9YCA6ICdTZXJ2ZXIgZXJyb3InO1xuICAgIGNvbnNvbGUuZXJyb3IoZXJyTXNnKTsgLy8gbG9nIHRvIGNvbnNvbGUgaW5zdGVhZFxuICAgIHJldHVybiBPYnNlcnZhYmxlLnRocm93KGVyck1zZyk7XG4gIH1cblxuXG59XG5cbiJdfQ==
