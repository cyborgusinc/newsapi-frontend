"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var index_1 = require('../shared/index');
var index_2 = require('../shared/index');
var ResourceComponent = (function () {
    function ResourceComponent(resourceListService, articlesListService, articlesPostService) {
        this.resourceListService = resourceListService;
        this.articlesListService = articlesListService;
        this.articlesPostService = articlesPostService;
    }
    ResourceComponent.prototype.ngOnInit = function () {
        this.getSources();
    };
    ResourceComponent.prototype.getSources = function () {
        var _this = this;
        this.resourceListService.get()
            .subscribe(function (sources) { return _this.sources = sources; }, function (error) { return _this.errorMessage = error; });
    };
    ResourceComponent.prototype.saveArticles = function () {
        var _this = this;
        var _loop_1 = function(source) {
            this_1.articlesListService.get(source)
                .subscribe(function (articles) { return _this.postArticles(source.name, articles); }, function (error) { return _this.errorMessage = error; });
        };
        var this_1 = this;
        for (var _i = 0, _a = this.selectedSources; _i < _a.length; _i++) {
            var source = _a[_i];
            _loop_1(source);
        }
    };
    ResourceComponent.prototype.postArticles = function (source, articles) {
        var _this = this;
        this.articlesPostService.post(source, articles)
            .subscribe(function (sources) { return _this.sources = sources; }, function (error) { return _this.errorMessage = error; });
    };
    ResourceComponent = __decorate([
        core_1.Component({
            moduleId: module.id,
            selector: 'sd-resource',
            templateUrl: 'resource.component.html',
            styleUrls: ['resource.component.css'],
        }), 
        __metadata('design:paramtypes', [index_1.ResourceListService, index_2.ArticleListService, index_2.ArticlesPostService])
    ], ResourceComponent);
    return ResourceComponent;
}());
exports.ResourceComponent = ResourceComponent;

//# sourceMappingURL=data:application/json;charset=utf8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFwcC9yZXNvdXJjZXMvcmVzb3VyY2UuY29tcG9uZW50LnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7Ozs7Ozs7QUFBQSxxQkFBa0MsZUFBZSxDQUFDLENBQUE7QUFDbEQsc0JBQW9DLGlCQUFpQixDQUFDLENBQUE7QUFDdEQsc0JBQXdELGlCQUFpQixDQUFDLENBQUE7QUFlMUU7SUFZRSwyQkFBbUIsbUJBQXdDLEVBQVMsbUJBQXVDLEVBQVMsbUJBQXdDO1FBQXpJLHdCQUFtQixHQUFuQixtQkFBbUIsQ0FBcUI7UUFBUyx3QkFBbUIsR0FBbkIsbUJBQW1CLENBQW9CO1FBQVMsd0JBQW1CLEdBQW5CLG1CQUFtQixDQUFxQjtJQUFHLENBQUM7SUFLaEssb0NBQVEsR0FBUjtRQUNFLElBQUksQ0FBQyxVQUFVLEVBQUUsQ0FBQztJQUNwQixDQUFDO0lBS0Qsc0NBQVUsR0FBVjtRQUFBLGlCQU1DO1FBTEMsSUFBSSxDQUFDLG1CQUFtQixDQUFDLEdBQUcsRUFBRTthQUMzQixTQUFTLENBQ1IsVUFBQyxPQUFrQixJQUFLLE9BQUEsS0FBSSxDQUFDLE9BQU8sR0FBRyxPQUFPLEVBQXRCLENBQXNCLEVBQzlDLFVBQUEsS0FBSyxJQUFLLE9BQUEsS0FBSSxDQUFDLFlBQVksR0FBUyxLQUFLLEVBQS9CLENBQStCLENBQzFDLENBQUM7SUFDTixDQUFDO0lBTU0sd0NBQVksR0FBbkI7UUFBQSxpQkFTQztRQVJDO1lBQ0UsTUFBSSxDQUFDLG1CQUFtQixDQUFDLEdBQUcsQ0FBQyxNQUFNLENBQUM7aUJBQ2pDLFNBQVMsQ0FDUixVQUFDLFFBQWtCLElBQUssT0FBQSxLQUFJLENBQUMsWUFBWSxDQUFDLE1BQU0sQ0FBQyxJQUFJLEVBQUUsUUFBUSxDQUFDLEVBQXhDLENBQXdDLEVBQ2hFLFVBQUEsS0FBSyxJQUFJLE9BQUEsS0FBSSxDQUFDLFlBQVksR0FBUyxLQUFLLEVBQS9CLENBQStCLENBQ3pDLENBQUM7OztRQUxOLEdBQUcsQ0FBQSxDQUFlLFVBQW9CLEVBQXBCLEtBQUEsSUFBSSxDQUFDLGVBQWUsRUFBcEIsY0FBb0IsRUFBcEIsSUFBb0IsQ0FBQztZQUFuQyxJQUFJLE1BQU0sU0FBQTs7U0FNYjtJQUVILENBQUM7SUFFTyx3Q0FBWSxHQUFwQixVQUFxQixNQUFjLEVBQUUsUUFBb0I7UUFBekQsaUJBTUM7UUFMQyxJQUFJLENBQUMsbUJBQW1CLENBQUMsSUFBSSxDQUFDLE1BQU0sRUFBRSxRQUFRLENBQUM7YUFDNUMsU0FBUyxDQUNSLFVBQUMsT0FBa0IsSUFBSyxPQUFBLEtBQUksQ0FBQyxPQUFPLEdBQUcsT0FBTyxFQUF0QixDQUFzQixFQUM5QyxVQUFBLEtBQUssSUFBSyxPQUFBLEtBQUksQ0FBQyxZQUFZLEdBQVMsS0FBSyxFQUEvQixDQUErQixDQUMxQyxDQUFDO0lBQ04sQ0FBQztJQTVESDtRQUFDLGdCQUFTLENBQUM7WUFDVCxRQUFRLEVBQUUsTUFBTSxDQUFDLEVBQUU7WUFDbkIsUUFBUSxFQUFFLGFBQWE7WUFDdkIsV0FBVyxFQUFFLHlCQUF5QjtZQUN0QyxTQUFTLEVBQUUsQ0FBQyx3QkFBd0IsQ0FBQztTQUN0QyxDQUFDOzt5QkFBQTtJQXlERix3QkFBQztBQUFELENBdkRBLEFBdURDLElBQUE7QUF2RFkseUJBQWlCLG9CQXVEN0IsQ0FBQSIsImZpbGUiOiJhcHAvcmVzb3VyY2VzL3Jlc291cmNlLmNvbXBvbmVudC5qcyIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IENvbXBvbmVudCwgT25Jbml0IH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XG5pbXBvcnQgeyBSZXNvdXJjZUxpc3RTZXJ2aWNlIH0gZnJvbSAnLi4vc2hhcmVkL2luZGV4JztcbmltcG9ydCB7IEFydGljbGVMaXN0U2VydmljZSwgQXJ0aWNsZXNQb3N0U2VydmljZSB9IGZyb20gJy4uL3NoYXJlZC9pbmRleCc7XG5pbXBvcnQge1NvdXJjZX0gZnJvbSBcIi4uLy4uL21vZGVscy9zb3VyY2VcIjtcbmltcG9ydCB7QXJ0aWNsZX0gZnJvbSBcIi4uLy4uL21vZGVscy9hcnRpY2xlXCI7XG5cblxuLyoqXG4gKiBUaGlzIGNsYXNzIHJlcHJlc2VudHMgdGhlIGxhenkgbG9hZGVkIEhvbWVDb21wb25lbnQuXG4gKi9cbkBDb21wb25lbnQoe1xuICBtb2R1bGVJZDogbW9kdWxlLmlkLFxuICBzZWxlY3RvcjogJ3NkLXJlc291cmNlJyxcbiAgdGVtcGxhdGVVcmw6ICdyZXNvdXJjZS5jb21wb25lbnQuaHRtbCcsXG4gIHN0eWxlVXJsczogWydyZXNvdXJjZS5jb21wb25lbnQuY3NzJ10sXG59KVxuXG5leHBvcnQgY2xhc3MgUmVzb3VyY2VDb21wb25lbnQgaW1wbGVtZW50cyBPbkluaXQge1xuICBlcnJvck1lc3NhZ2U6IHN0cmluZztcbiAgcHVibGljIHNvdXJjZXM6IFNvdXJjZVtdO1xuICBwdWJsaWMgc2VsZWN0ZWRTb3VyY2VzOiBTb3VyY2VbXTtcblxuXG4gIC8qKlxuICAgKiBDcmVhdGVzIGFuIGluc3RhbmNlIG9mIHRoZSBSZXNvdXJjZUNvbXBvbmVudCB3aXRoIHRoZSBpbmplY3RlZFxuICAgKiBOYW1lTGlzdFNlcnZpY2UuXG4gICAqXG4gICAqIEBwYXJhbSB7TmFtZUxpc3RTZXJ2aWNlfSByZXNvdXJjZUxpc3RTZXJ2aWNlIC0gVGhlIGluamVjdGVkIE5hbWVMaXN0U2VydmljZS5cbiAgICovXG4gIGNvbnN0cnVjdG9yKHB1YmxpYyByZXNvdXJjZUxpc3RTZXJ2aWNlOiBSZXNvdXJjZUxpc3RTZXJ2aWNlLCBwdWJsaWMgYXJ0aWNsZXNMaXN0U2VydmljZTogQXJ0aWNsZUxpc3RTZXJ2aWNlLCBwdWJsaWMgYXJ0aWNsZXNQb3N0U2VydmljZTogQXJ0aWNsZXNQb3N0U2VydmljZSkge31cblxuICAvKipcbiAgICogR2V0IHRoZSBuYW1lcyBPbkluaXRcbiAgICovXG4gIG5nT25Jbml0KCkge1xuICAgIHRoaXMuZ2V0U291cmNlcygpO1xuICB9XG5cbiAgLyoqXG4gICAqIEhhbmRsZSB0aGUgcmVzb3VyY2VMaXN0U2VydmljZSBvYnNlcnZhYmxlXG4gICAqL1xuICBnZXRTb3VyY2VzKCkge1xuICAgIHRoaXMucmVzb3VyY2VMaXN0U2VydmljZS5nZXQoKVxuICAgICAgLnN1YnNjcmliZShcbiAgICAgICAgKHNvdXJjZXMgOiBTb3VyY2VbXSkgPT4gdGhpcy5zb3VyY2VzID0gc291cmNlcyxcbiAgICAgICAgZXJyb3IgPT4gIHRoaXMuZXJyb3JNZXNzYWdlID0gPGFueT4gZXJyb3IsXG4gICAgICApO1xuICB9XG5cbiAgLyoqXG4gICAqIFB1c2hlcyBhIG5ldyBuYW1lIG9udG8gdGhlIG5hbWVzIGFycmF5XG4gICAqIEByZXR1cm4ge2Jvb2xlYW59IGZhbHNlIHRvIHByZXZlbnQgZGVmYXVsdCBmb3JtIHN1Ym1pdCBiZWhhdmlvciB0byByZWZyZXNoIHRoZSBwYWdlLlxuICAgKi9cbiAgcHVibGljIHNhdmVBcnRpY2xlcygpe1xuICAgIGZvcihsZXQgc291cmNlIG9mIHRoaXMuc2VsZWN0ZWRTb3VyY2VzKSB7XG4gICAgICB0aGlzLmFydGljbGVzTGlzdFNlcnZpY2UuZ2V0KHNvdXJjZSlcbiAgICAgICAgLnN1YnNjcmliZShcbiAgICAgICAgICAoYXJ0aWNsZXM6QXJ0aWNsZVtdKSA9PiB0aGlzLnBvc3RBcnRpY2xlcyhzb3VyY2UubmFtZSwgYXJ0aWNsZXMpLFxuICAgICAgICAgIGVycm9yID0+IHRoaXMuZXJyb3JNZXNzYWdlID0gPGFueT4gZXJyb3IsXG4gICAgICAgICk7XG4gICAgfVxuXG4gIH1cblxuICBwcml2YXRlIHBvc3RBcnRpY2xlcyhzb3VyY2U6IHN0cmluZywgYXJ0aWNsZXMgOiBBcnRpY2xlW10pe1xuICAgIHRoaXMuYXJ0aWNsZXNQb3N0U2VydmljZS5wb3N0KHNvdXJjZSwgYXJ0aWNsZXMpXG4gICAgICAuc3Vic2NyaWJlKFxuICAgICAgICAoc291cmNlcyA6IFNvdXJjZVtdKSA9PiB0aGlzLnNvdXJjZXMgPSBzb3VyY2VzLFxuICAgICAgICBlcnJvciA9PiAgdGhpcy5lcnJvck1lc3NhZ2UgPSA8YW55PiBlcnJvcixcbiAgICAgICk7XG4gIH1cblxufVxuIl19
