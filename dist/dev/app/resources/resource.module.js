"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var common_1 = require('@angular/common');
var shared_module_1 = require('../shared/shared.module');
var resource_component_1 = require('./resource.component');
var index_1 = require('../shared/resource-list/index');
var article_list_service_1 = require("../shared/article-list/article-list.service");
var ResourceModule = (function () {
    function ResourceModule() {
    }
    ResourceModule = __decorate([
        core_1.NgModule({
            imports: [common_1.CommonModule, shared_module_1.SharedModule],
            declarations: [resource_component_1.ResourceComponent],
            exports: [resource_component_1.ResourceComponent],
            providers: [index_1.ResourceListService, article_list_service_1.ArticleListService]
        }), 
        __metadata('design:paramtypes', [])
    ], ResourceModule);
    return ResourceModule;
}());
exports.ResourceModule = ResourceModule;

//# sourceMappingURL=data:application/json;charset=utf8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFwcC9yZXNvdXJjZXMvcmVzb3VyY2UubW9kdWxlLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7Ozs7Ozs7QUFBQSxxQkFBeUIsZUFBZSxDQUFDLENBQUE7QUFDekMsdUJBQTZCLGlCQUFpQixDQUFDLENBQUE7QUFDL0MsOEJBQTZCLHlCQUF5QixDQUFDLENBQUE7QUFDdkQsbUNBQWtDLHNCQUFzQixDQUFDLENBQUE7QUFDekQsc0JBQW9DLCtCQUErQixDQUFDLENBQUE7QUFDcEUscUNBQWlDLDZDQUE2QyxDQUFDLENBQUE7QUFRL0U7SUFBQTtJQUE4QixDQUFDO0lBTi9CO1FBQUMsZUFBUSxDQUFDO1lBQ1IsT0FBTyxFQUFFLENBQUMscUJBQVksRUFBRSw0QkFBWSxDQUFDO1lBQ3JDLFlBQVksRUFBRSxDQUFDLHNDQUFpQixDQUFDO1lBQ2pDLE9BQU8sRUFBRSxDQUFDLHNDQUFpQixDQUFDO1lBQzVCLFNBQVMsRUFBRSxDQUFDLDJCQUFtQixFQUFFLHlDQUFrQixDQUFDO1NBQ3JELENBQUM7O3NCQUFBO0lBQzRCLHFCQUFDO0FBQUQsQ0FBOUIsQUFBK0IsSUFBQTtBQUFsQixzQkFBYyxpQkFBSSxDQUFBIiwiZmlsZSI6ImFwcC9yZXNvdXJjZXMvcmVzb3VyY2UubW9kdWxlLmpzIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgTmdNb2R1bGUgfSBmcm9tICdAYW5ndWxhci9jb3JlJztcbmltcG9ydCB7IENvbW1vbk1vZHVsZSB9IGZyb20gJ0Bhbmd1bGFyL2NvbW1vbic7XG5pbXBvcnQgeyBTaGFyZWRNb2R1bGUgfSBmcm9tICcuLi9zaGFyZWQvc2hhcmVkLm1vZHVsZSc7XG5pbXBvcnQgeyBSZXNvdXJjZUNvbXBvbmVudCB9IGZyb20gJy4vcmVzb3VyY2UuY29tcG9uZW50JztcbmltcG9ydCB7IFJlc291cmNlTGlzdFNlcnZpY2UgfSBmcm9tICcuLi9zaGFyZWQvcmVzb3VyY2UtbGlzdC9pbmRleCc7XG5pbXBvcnQge0FydGljbGVMaXN0U2VydmljZX0gZnJvbSBcIi4uL3NoYXJlZC9hcnRpY2xlLWxpc3QvYXJ0aWNsZS1saXN0LnNlcnZpY2VcIjtcblxuQE5nTW9kdWxlKHtcbiAgaW1wb3J0czogW0NvbW1vbk1vZHVsZSwgU2hhcmVkTW9kdWxlXSxcbiAgZGVjbGFyYXRpb25zOiBbUmVzb3VyY2VDb21wb25lbnRdLFxuICBleHBvcnRzOiBbUmVzb3VyY2VDb21wb25lbnRdLFxuICBwcm92aWRlcnM6IFtSZXNvdXJjZUxpc3RTZXJ2aWNlLCBBcnRpY2xlTGlzdFNlcnZpY2VdXG59KVxuZXhwb3J0IGNsYXNzIFJlc291cmNlTW9kdWxlIHsgfVxuIl19
