import { Component, OnInit } from '@angular/core';
import { ResourceListService } from '../shared/index';
import { ArticleListService, ArticlesPostService } from '../shared/index';
import {Source} from "../../models/source";
import {Article} from "../../models/article";


/**
 * This class represents the lazy loaded HomeComponent.
 */
@Component({
  moduleId: module.id,
  selector: 'sd-resource',
  templateUrl: 'resource.component.html',
  styleUrls: ['resource.component.css'],
})

export class ResourceComponent implements OnInit {
  errorMessage: string;
  public sources: Source[];
  public selectedSources: Source[];


  /**
   * Creates an instance of the ResourceComponent with the injected
   * NameListService.
   *
   * @param {NameListService} resourceListService - The injected NameListService.
   */
  constructor(public resourceListService: ResourceListService, public articlesListService: ArticleListService, public articlesPostService: ArticlesPostService) {}

  /**
   * Get the names OnInit
   */
  ngOnInit() {
    this.getSources();
  }

  /**
   * Handle the resourceListService observable
   */
  getSources() {
    this.resourceListService.get()
      .subscribe(
        (sources : Source[]) => this.sources = sources,
        error =>  this.errorMessage = <any> error,
      );
  }

  /**
   * Pushes a new name onto the names array
   * @return {boolean} false to prevent default form submit behavior to refresh the page.
   */
  public saveArticles(){
    for(let source of this.selectedSources) {
      this.articlesListService.get(source)
        .subscribe(
          (articles:Article[]) => this.postArticles(source.name, articles),
          error => this.errorMessage = <any> error,
        );
    }

  }

  private postArticles(source: string, articles : Article[]){
    this.articlesPostService.post(source, articles)
      .subscribe(
        (sources : Source[]) => this.sources = sources,
        error =>  this.errorMessage = <any> error,
      );
  }

}
