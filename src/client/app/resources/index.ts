/**
 * This barrel file provides the export for the lazy loaded ResourceComponent.
 */
export * from './resource.component';
export * from './resource.routes';
