import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from '../shared/shared.module';
import { ResourceComponent } from './resource.component';
import { ResourceListService } from '../shared/resource-list/index';
import {ArticleListService} from "../shared/article-list/article-list.service";

@NgModule({
  imports: [CommonModule, SharedModule],
  declarations: [ResourceComponent],
  exports: [ResourceComponent],
  providers: [ResourceListService, ArticleListService]
})
export class ResourceModule { }
