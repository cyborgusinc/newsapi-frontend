import { Routes } from '@angular/router';

import { ResourceRoutes } from './resources/index';

export const routes: Routes = [
  ...ResourceRoutes
];
