import { Injectable } from '@angular/core';
import {Http, Response, Headers, RequestOptions} from '@angular/http';
import { Observable } from 'rxjs/Observable';

import 'rxjs/add/observable/throw';
import {Source} from "../../../models/source";

/**
 * This class provides the NameList service with methods to read names and add names.
 */
@Injectable()
export class ResourceListService {

  /**
   * Creates a new NameListService with the injected Http.
   * @param {Http} http - The injected Http.
   * @constructor
   */
  constructor(private http: Http) {}

  /**
   * Returns an Observable for the HTTP GET request for the JSON resource.
   * @return {string[]} The Observable for the HTTP request.
   */
  get(): Observable<Source[]> {
    return this.http.get('https://newsapi.org/v1/sources?language=en')
      .map((res: Response) => <Source[]> res.json().sources)
      //              .do(data => console.log('server data:', data))  // debug
      .catch(this.handleError);
  }

  post(sources: Source[]): Observable<Source[]> {
    let headers = new Headers();
    headers.append('Content-Type', 'application/json');
    headers.append('Access-Control-Allow-Origin', 'http://localhost:8080/articles');
    let options = new RequestOptions({ headers: headers })
    return this.http.post('http://localhost:8080/articles', sources, options)
      .map((res: Response) => <Source[]> res.json().sources)
      //              .do(data => console.log('server data:', data))  // debug
      .catch(this.handleError);
  }


  /**
   * Handle HTTP error
   */
  private handleError (error: any) {
    // In a real world app, we might use a remote logging infrastructure
    // We'd also dig deeper into the error to get a better message
    let errMsg = (error.message) ? error.message :
      error.status ? `${error.status} - ${error.statusText}` : 'Server error';
    console.error(errMsg); // log to console instead
    return Observable.throw(errMsg);
  }


}

